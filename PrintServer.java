package doevent.print;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

import java.io.BufferedReader; 
import java.io.BufferedWriter;
import java.io.InputStreamReader; 
import java.io.OutputStreamWriter;



/**
 * A TCP server that runs on port 9090.  When a client connects, it
 * sends the client the current date and time, then closes the
 * connection with that client.  Arguably just about the simplest
 * server you can write.
 */
public class PrintServer {

    /**
     * Runs the server.
     */
    public static void main(String[] args) throws IOException {
        ServerSocket listener = new ServerSocket(9090);
        System.out.println("Server start port 9090 ....");
        try {
            while (true) {
                Socket clientSocket = listener.accept();
                try {
                    
                    InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream()); 
                    BufferedReader reader = new BufferedReader(isr); 
                    // BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                    // String line = reader.readLine(); 
                    // try {
                    //     while (!line.isEmpty()) { 
                            
                    //         line = reader.readLine(); 
                    //     }
                    // }catch(Exception e){
                    //     System.out.println( e);
                    // }
                    
                } finally {
                    System.out.println("Closing Client");
                    clientSocket.close();                    
                }
            }
        }
        finally {
            System.out.println("Closing listener");
            listener.close();
        }
    }

    private static void cetakBrowser(BufferedWriter out, String str)
    {
        try {
            out.write("HTTP/1.0 200 OK\r\n");
            out.write("Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n");
            out.write("Server: Apache/0.8.4\r\n");
            out.write("Content-Type: text/html\r\n");
            out.write("Content-Length: 59\r\n");
            out.write("Expires: Sat, 01 Jan 2000 00:59:59 GMT\r\n");
            out.write("Last-modified: Fri, 09 Aug 1996 14:21:40 GMT\r\n");
            out.write("\r\n");
            out.write("<TITLE>Exemple</TITLE>");
            out.write("<P>Ceci est une page d'exemple.</P>");
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
}